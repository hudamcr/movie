## 1.00.1 (10001)
*Release (17 Sep 2023)*

**Features :**
* Homepage
* Genre
* Movie List
* Movie Detail
* Trailer
* Review

**Improvements :**

**Bugfixes :**